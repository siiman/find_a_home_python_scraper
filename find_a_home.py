import requests
from bs4 import BeautifulSoup
from tinydb import TinyDB, Query
import time
from jinja2 import Environment, FileSystemLoader, select_autoescape


def scrape(urls):
    """
    Scrape webpage for apartments. Sample HTML structure:
    <tbody>
        <tr>
            <td>
                <div class="nr">1</div>
            </td>
            <td data-sort-value="1">1</td>
            <td data-sort-value="3">3</td>
            <td data-sort-value="65.2">
                <div class="size">65.2 </div>
            </td>
            <td data-sort-value="28.6">
                <div class="balcony">28.6 </div>
            </td>
            <td data-sort-value="0">
                <div class="price">Sold</div>
            </td>
        </tr>
    </tbody>

    :param urls: list of dictionaries with keys 'url' and 'address'
    :return: list of dictionaries containing data about scraped apartments
    """
    scraped_apartments = []

    for u in urls:
        page = requests.get(u["url"])
        soup = BeautifulSoup(page.content, "html.parser")
        res = soup.find("tbody")
        apartments_on_page = res.find_all("tr")
        for a in apartments_on_page:
            apartment_nr = a.select_one("td:nth-of-type(1)").get_text(strip=True)
            floor = int(a.select_one("td:nth-of-type(2)").get_text(strip=True))
            rooms = int(a.select_one("td:nth-of-type(3)").get_text(strip=True))
            square_meters = float(a.select_one("td:nth-of-type(4)").get_text(strip=True).replace(",", "."))
            price = a.select_one("td:nth-of-type(6)").get_text(strip=True)
            if square_meters > 65 and floor > 1 and rooms == 3:
                scraped_apartments.append({
                    "address": u["address"],
                    "apartment_nr": apartment_nr,
                    "floor": floor,
                    "rooms": rooms,
                    "square_meters": square_meters,
                    "price": price
                })

    return scraped_apartments


def update_db(scraped_apartments, db_instance, query):
    """
    Upsert data and add time when the program discovered that an apartment was sold

    :param scraped_apartments: list of dictionaries scraped with scrape() function
    :param db_instance: TinyDB() object
    :param query: Query() object
    :return: list of recently sold apartment's ID-s
    """
    for a in scraped_apartments:
        db_instance.upsert(a, (query.apartment_nr == a["apartment_nr"]) & (query.address == a["address"]))

    got_sold = db_instance.update({"when_sold": time.strftime("%Y-%m-%d %H:%M:%S")},
                                  (query.price.matches("Booked|Sold") & ~(query.when_sold.exists())))

    return got_sold


def generate_report(sold, available):
    """
    Generate HTML report of recently sold and still available apartments

    :param sold: list of sold apartments queried from the db
    :param available: list of available apartments queried from the db
    """
    loader = FileSystemLoader("./")
    env = Environment(loader=loader, autoescape=select_autoescape(default=True))
    template = env.get_template("report.html.j2")
    output_html = template.render(sold_after_last_run=sold, available_apartments=available)
    time_prefix = time.strftime("%Y-%m-%d_%H:%M:%S")

    with open(f"reports/report_{time_prefix}.html", "w") as f:
        f.write(output_html)


if __name__ == "__main__":
    # Following values redacted 'cause y'know privacy ;)
    urls = [
        {"url": "", "address": ""},
        {"url": "", "address": ""}
    ]
    sold_after_last_run = []

    with TinyDB("./apartments.json") as db:
        q = Query()
        results = scrape(urls)
        sold_ids = update_db(results, db, q)
        for i in sold_ids:
            sold_after_last_run.append(db.get(doc_id=i))
        available_apartments = db.search(~(q.when_sold.exists()))

    generate_report(sold_after_last_run, available_apartments)
