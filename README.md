Everyone's gotta have a Python web scrapper I guess. :smile:  So this simple one just scrapes some really top secret real estate portals for suitable apartments. 

- Beautiful Soup for scraping

- TinyDB for persistent storage

- Jinja2 for generating reports